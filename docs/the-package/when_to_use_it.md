# WHEN TO USE IT?

To perform several action on a local or remote Mysql server, like:

* executing queries.
* modeling database schema's.
* monitor and manage the server.
* reverse engineering.
* and much more... the list is endless. Please refer to docs for more details.


---

[<< previous](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/what_is_it.md) | [next >>](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/install.md)

[HOME](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/README.md)
